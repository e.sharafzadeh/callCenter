class Result
  def initialize(table_result, caller_delay, time_in_sys, time_population, percentages)
    @table_result = table_result
    @caller_delay = caller_delay
    @time_in_sys = time_in_sys
    @time_population = time_population
    @percentages = percentages
  end
  attr_accessor :caller_delay, :table_result, :time_in_sys, :time_population, :percentages
end